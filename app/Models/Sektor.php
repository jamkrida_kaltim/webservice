<?php

namespace App\Models;

use App\Models\Base as Model;

class Sektor extends Model
{
    protected $table = 'sektor';

    protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];
}
