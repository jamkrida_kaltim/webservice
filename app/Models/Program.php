<?php

namespace App\Models;

use App\Models\Base as Model;

class Program extends Model
{
    protected $table = 'program';

    protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];
}
