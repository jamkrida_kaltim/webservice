<?php

namespace App\Models;

use App\Models\Base as Model;

class Tarif extends Model
{
    protected $table = 'tarif';

    protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];
}
