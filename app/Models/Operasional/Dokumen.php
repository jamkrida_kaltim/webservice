<?php

namespace App\Models\Operasional;

use App\Models\Base as Model;

class Dokumen extends Model
{
    protected $table = 'dokumen';

    protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];
}
