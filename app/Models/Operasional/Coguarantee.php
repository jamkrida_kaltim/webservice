<?php

namespace App\Models\Operasional;

use App\Models\Base as Model;

class Coguarantee extends Model
{
    protected $table = 'coguarantee';

    protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];

    public function getTanggalPembayaranAttribute()
    {
        if(filled($this->attributes['tanggal_pembayaran'])){
            return Carbon::parse($this->attributes['tanggal_pembayaran'])->format('d/m/Y');
        }
    }

    public function created_by()
    {
        return $this->belongsTo(\App\User::class,'created_user_id', 'id');
    }

    public function updated_by()
    {
        return $this->belongsTo(\App\User::class,'updated_user_id', 'id');
    }
}
