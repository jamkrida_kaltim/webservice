<?php

namespace App\Models\Operasional;

use App\Models\Base as Model;

class Score extends Model
{
    protected $table = 'score';

    protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];
}
