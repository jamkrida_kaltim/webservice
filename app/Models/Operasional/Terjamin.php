<?php

namespace App\Models\Operasional;

use App\Models\Base as Model;
use Carbon\Carbon;

class Terjamin extends Model
{
    protected $table = 'terjamin';

    protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];

    public function getTanggalLahirAttribute()
    {
        if(filled($this->attributes['tanggal_lahir'])){
            return Carbon::parse($this->attributes['tanggal_lahir'])->format('d/m/Y');
        }
    }
}
