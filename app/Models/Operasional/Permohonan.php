<?php

namespace App\Models\Operasional;

use App\Models\Base as Model;
use Carbon\Carbon;

class Permohonan extends Model
{
    protected $table = 'permohonan';

    protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];

    public function scopeNew($query)
    {
        return $query->where('status', 0);
    }

    public function scopeDone($query)
    {
        return $query->where('status', 1);
    }

    public function scopeProcess($query)
    {
        return $query->where('status', 2);
    }

    public function scopeKredit($query)
    {
        return $query->whereIn('produk_id', [1,2,3,4]);
    }

    public function scopekbg($query)
    {
        return $query->where('produk_id', 5);
    }
    
    public function scopeSbd($query)
    {
        return $query->where('produk_id', 6);
    }

    public function getTanggalAttribute()
    {
        if(filled($this->attributes['tanggal'])){
            return Carbon::parse($this->attributes['tanggal'])->format('d/m/Y');
        }
    }

    public function getAgendaTanggalAttribute()
    {
        if(filled($this->attributes['agenda_tanggal'])){
            return Carbon::parse($this->attributes['agenda_tanggal'])->format('d/m/Y');
        }
    }

    public function lembaga_keuangan()
    {
        return $this->belongsTo(\App\Models\LembagaKeuangan::class, 'pemohonan_id', 'id');
    }
}
