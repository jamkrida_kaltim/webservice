<?php

namespace App\Models\Operasional;

use App\Models\Base as Model;
use Carbon\Carbon;

class Sertifikat extends Model
{
    protected $table = 'sertifikat';

    protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];

    public function getAgendaTanggalAttribute()
    {
        if(filled($this->attributes['agenda_tanggal'])){
            return Carbon::parse($this->attributes['agenda_tanggal'])->format('d/m/Y');
        }
    }

    public function getTanggalTerbitAttribute()
    {
        if(filled($this->attributes['tanggal_terbit'])){
            return Carbon::parse($this->attributes['tanggal_terbit'])->format('d/m/Y');
        }
    }
    public function getTanggalPertemuanAttribute()
    {
        if(filled($this->attributes['tanggal_pertemuan'])){
            return Carbon::parse($this->attributes['tanggal_pertemuan'])->format('d/m/Y');
        }
    }

    public function permohonan()
    {
        return $this->belongsTo(Permohonan::class);
    }

    public function penjaminan()
    {
        return $this->hasMany(Penjaminan\Penjaminan::class);
    }

    public function first_sign()
    {
        return $this->belongsTo(\App\Models\Pegawai\Pegawai::class, 'first_sign_id', 'id');
    }

    public function second_sign()
    {
        return $this->belongsTo(\App\Models\Pegawai\Pegawai::class, 'second_sign_id', 'id');
    }

    public function direktur_utama()
    {
        return $this->belongsTo(\App\Models\Pegawai\Pegawai::class, 'direktur_utama_id', 'id');
    }
    
    public function direktur_operasional()
    {
        return $this->belongsTo(\App\Models\Pegawai\Pegawai::class, 'direktur_operasional_id', 'id');
    }

    public function direktur_umum()
    {
        return $this->belongsTo(\App\Models\Pegawai\Pegawai::class, 'direktur_umum_id', 'id');
    }

    public function kepala_bagian()
    {
        return $this->belongsTo(\App\Models\Pegawai\Pegawai::class, 'kepala_bagian_id', 'id');
    }
}
