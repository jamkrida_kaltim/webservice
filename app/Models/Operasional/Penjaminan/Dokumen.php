<?php

namespace App\Models\Operasional\Penjaminan;

use App\Models\Base as Model;

class Dokumen extends Model
{
    protected $table = 'penjaminan_dokumen';

    protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];
}
