<?php

namespace App\Models\Operasional\Penjaminan;

use App\Models\Base as Model;

class Performance extends Model
{
    protected $table = 'penjaminan_performance';

    protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];
}
