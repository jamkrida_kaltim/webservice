<?php

namespace App\Models\Operasional\Penjaminan;

use App\Models\Base as Model;

class Biaya extends Model
{
    protected $table = 'penjaminan_biaya';

    protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];
}
