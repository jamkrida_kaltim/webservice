<?php

namespace App\Models\Operasional\Penjaminan;

use App\Models\Base as Model;
use Carbon\Carbon;

class Penjaminan extends Model
{
    protected $table = 'penjaminan';

    protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];

    public function scopeNew($query)
    {
        return $query->where('status', 0);
    }

    public function scopeValid($query)
    {
        return $query->where('status', 1);
    }

    public function scopeProcces($query)
    {
        return $query->where('status', 2);
    }

    public function scopeCancel($query)
    {
        return $query->where('status', 3);
    }

    // public function scopePress($query)
    // {
    //     return $query->where('status', 4);
    // }

    // public function scopeDone($query)
    // {
    //     return $query->where('status', 5);
    // }

    public function getTanggalAttribute()
    {
        if(filled($this->attributes['tanggal'])){
            return Carbon::parse($this->attributes['tanggal'])->format('d/m/Y');
        }
    }

    public function getPermohonanTanggalAttribute()
    {
        if(filled($this->attributes['permohonan_tanggal'])){
            return Carbon::parse($this->attributes['permohonan_tanggal'])->format('d/m/Y');
        }
    }

    public function getSertifikatTanggalAttribute()
    {
        if(filled($this->attributes['sertifikat_tanggal'])){
            return Carbon::parse($this->attributes['sertifikat_tanggal'])->format('d/m/Y');
        }
    }

    public function getImbalJasaTanggalTerimaAttribute()
    {
        if(filled($this->attributes['imbal_jasa_tanggal_terima'])){
            return Carbon::parse($this->attributes['imbal_jasa_tanggal_terima'])->format('d/m/Y');
        }
    }

    public function getTanggalJatuhTempoAttribute()
    {
        if(filled($this->attributes['tanggal_jatuh_tempo'])){
            return Carbon::parse($this->attributes['tanggal_jatuh_tempo'])->format('d/m/Y');
        }
    }

    public function getTanggalMulaiAttribute()
    {
        if(filled($this->attributes['tanggal_mulai'])){
            return Carbon::parse($this->attributes['tanggal_mulai'])->format('d/m/Y');
        }
    }

    public function getBankGaransiTanggalAttribute()
    {
        if(filled($this->attributes['bank_garansi_tanggal'])){
            return Carbon::parse($this->attributes['bank_garansi_tanggal'])->format('d/m/Y');
        }
    }

    public function getTerjaminTanggalLahirAttribute()
    {
        if(filled($this->attributes['terjamin_tanggal_lahir'])){
            return Carbon::parse($this->attributes['terjamin_tanggal_lahir'])->format('d/m/Y');
        }
    }

    public function terjamin()
    {
        return $this->belongsTo(\App\Models\Operasional\Terjamin::class, 'terjamin_id', 'id');
    }
}
