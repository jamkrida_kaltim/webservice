<?php

namespace App\Models\Operasional\Penjaminan;

use App\Models\Base as Model;

class SPPP extends Model
{
    protected $table = 'penjaminan_sppp';

    protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];

    public function first_sign()
    {
        return $this->belongsTo(\App\Models\Pegawai\Pegawai::class, 'first_sign_id', 'id');
    }

    public function second_sign()
    {
        return $this->belongsTo(\App\Models\Pegawai\Pegawai::class, 'second_sign_id', 'id');
    }
}
