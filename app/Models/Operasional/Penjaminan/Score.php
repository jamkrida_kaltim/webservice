<?php

namespace App\Models\Operasional\Penjaminan;

use App\Models\Base as Model;

class Score extends Model
{
    protected $table = 'penjaminan_score';

    protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];
}
