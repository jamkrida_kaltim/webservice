<?php

namespace App\Models;

use App\Models\Base as Model;

class Produk extends Model
{
    protected $table = 'produk';

    protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];

    public function scopeKeyword($query, $keyword)
    {
        return $query->where('nama', 'like', '%'.$keyword.'%');
    }
}
