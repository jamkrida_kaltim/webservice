<?php

namespace App\Models;

use App\Models\Base as Model;
use Carbon\Carbon;

class PerjanjianKerjasama extends Model
{
    protected $table = 'perjanjian_kerjasama';

    protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];

    public function getTanggalMulaiAttribute()
    {
        if(filled($this->attributes['tanggal_mulai'])){
            return Carbon::parse($this->attributes['tanggal_mulai'])->format('d/m/Y');
        }
    }

    public function getTanggalAkhirAttribute()
    {
        if(filled($this->attributes['tanggal_akhir'])){
            return Carbon::parse($this->attributes['tanggal_akhir'])->format('d/m/Y');
        }
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function lembaga()
    {
        return $this->belongsTo(LembagaKeuangan::class, 'lembaga_keuangan_id', 'id');
    }
}
