<?php

namespace App\Models;

use App\Models\Base as Model;

class LembagaKeuangan extends Model
{
    protected $table = 'lembaga_keuangan';

    protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];

    public function getFullNameAttribute()
    {
        return $this->nama_perusahaan." ".$this->kantor." ".$this->nama;
    }

    public function getFullTypeAttribute()
    {
        if($this->type == 'penerima_jaminan'){
            $type = 'Penerima Jaminan';
        }else{
            $type = 'Reasuransi';
        }

        return  $type;
    }

    public function getFullKategoriAttribute()
    {
        if($this->kategori == 'non-bank'){
            $kategori = 'Non Bank/Koperasi';
        }else{
            $kategori = 'Bank';
        }

        return  $kategori;
    }

    public function scopePenerima_jaminan($query)
    {
        return $query->where('type', 'penerima_jaminan');
    }
    
    public function scopeReasuransi($query)
    {
        return $query->where('type', 'reasuransi');
    }

    public function scopeBank($query)
    {
        return $query->where('kategori', 'bank');
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function scopeNonBank($query)
    {
        return $query->where('kategori', 'non-bank');
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'type_id', 'id');
    }

    public function kota()
    {
        return $this->belongsTo(\App\Models\Kota::class, 'kota_id', 'id');
    }
    
}
