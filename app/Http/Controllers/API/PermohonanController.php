<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Tarif;
use App\Models\Sektor;
use App\Models\Plafond;
use App\Models\Program;
use App\Models\Produk;
use App\Models\LembagaKeuangan;
use App\Models\PerjanjianKerjasama;
use App\Models\Operasional\Permohonan;
use App\Models\Operasional\Penjaminan\Penjaminan;
use App\Models\Operasional\Coguarantee;
use App\Models\Operasional\Terjamin;
use Auth;

class PermohonanController extends Controller
{
    public function permohonan()
    {
        return Auth::user()->username;
    }

    public function penjaminan()
    {
        return Auth::user()->username;
    }
}
