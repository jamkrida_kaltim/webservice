# SIJAMIN - WEB SERVICE

## Getting Started

This application can be installed on local server and online server with these specifications :

#### Server Requirements

1. PHP >= 8 (and meet [Laravel 8 server requirements](https://laravel.com/docs/8.x/deployment#server-requirements)),

#### Installation Steps

1. Clone the repo : `git clone git@bitbucket.org:jamkridakaltim/webservice.git`
2. `$ cd webservice`
3. `$ composer install`
4. `$ cp .env.example .env`
